package me.fidelep.albosegundaparte

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room.databaseBuilder
import me.fidelep.albosegundaparte.data.api.BeerService
import me.fidelep.albosegundaparte.data.db.BeerRoom

class MainActivity : AppCompatActivity() {
    companion object {
        lateinit var database: BeerRoom
        lateinit var beerService: BeerService
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        database = databaseBuilder(this, BeerRoom::class.java, "beer_db").build()

        beerService = BeerService()
    }
}