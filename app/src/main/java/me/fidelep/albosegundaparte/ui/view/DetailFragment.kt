package me.fidelep.albosegundaparte.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_detail.*
import me.fidelep.albosegundaparte.R
import me.fidelep.albosegundaparte.data.model.Beer

class DetailFragment : Fragment() {

    private val args: DetailFragmentArgs by navArgs()
    lateinit var beer: Beer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        beer = args.Beer
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.title = beer.name
        context?.let {
            Glide.with(it).load(beer.image).placeholder(R.drawable.ic_beer)
                .into(image)
        }
        lblTagline.text = beer.tagline
        firstBrewedDate.text = beer.firstBrewed
        lblDescription.text = beer.description

        val pairingBuilder = StringBuilder()

        Gson().fromJson(beer.foodPairing, mutableListOf<String>()::class.java)?.forEach {
            pairingBuilder.append("· $it \n")
        }
        pairingData.text = pairingBuilder
    }

}