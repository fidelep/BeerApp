package me.fidelep.albosegundaparte.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.beer_item.view.*
import me.fidelep.albosegundaparte.R
import me.fidelep.albosegundaparte.data.model.Beer
import me.fidelep.albosegundaparte.ui.view.MasterFragmentDirections


class BeerListAdapter(var beers: ArrayList<Beer>) :
    RecyclerView.Adapter<BeerListAdapter.BeerViewHolder>() {

    class BeerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val image = view.image
        private val name = view.lblName
        private val tagline = view.lblTagline
        fun bind(beer: Beer) {
            name.text = beer.name
            tagline.text = beer.tagline
            Glide.with(itemView.context).load(beer.image).placeholder(R.drawable.ic_beer)
                .into(image)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) = BeerViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.beer_item, parent, false)
    )

    override fun getItemCount(): Int = beers.size

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        holder.bind(beers[position])
        holder.apply {
            itemView.setOnClickListener {
                Navigation.findNavController(itemView).let {
                    val action = MasterFragmentDirections.toDetailFragment(beers[position])
                    it.navigate(action)
                }
            }
        }
    }

    fun updateBeers(newBeers: List<Beer>) {
        beers.clear()
        appendBeers(newBeers)
    }

    fun appendBeers(newBeers: List<Beer>) {
        beers.addAll(newBeers)
        notifyDataSetChanged()
    }
}