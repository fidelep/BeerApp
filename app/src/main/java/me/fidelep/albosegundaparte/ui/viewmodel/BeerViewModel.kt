package me.fidelep.albosegundaparte.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.fidelep.albosegundaparte.data.model.Beer
import me.fidelep.albosegundaparte.data.repository.BeerRepository

class BeerViewModel(private val repository: BeerRepository) : ViewModel() {

    private val ITEMS_AMOUNT = 20
    private var pageNumber = 1

    val beers = MutableLiveData<List<Beer>>()
    val isLoading = MutableLiveData<Boolean>()

    init {
        beers.value = emptyList()
    }

    fun start() {
        pageNumber = 1
        fetchBeers()
    }


    private fun fetchBeers() {
        isLoading.value = true
        viewModelScope.launch {

            repository.getBeersByPage(pageNumber, ITEMS_AMOUNT).observeForever {
                if (repository.isLocal) {
                    beers.value = it
                } else {
                    val newList = beers.value!!.toMutableList()
                    newList.addAll(it)
                    beers.value = newList
                }
                isLoading.value = false
            }

        }
    }

    fun nextPage() {
        ++pageNumber
        fetchBeers()
    }

}