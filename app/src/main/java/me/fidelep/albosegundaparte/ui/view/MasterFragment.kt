package me.fidelep.albosegundaparte.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_master.*
import me.fidelep.albosegundaparte.MainActivity
import me.fidelep.albosegundaparte.R
import me.fidelep.albosegundaparte.data.repository.BeerRepositoryImpl
import me.fidelep.albosegundaparte.ui.adapter.BeerListAdapter
import me.fidelep.albosegundaparte.ui.viewmodel.BeerViewModel

class MasterFragment : Fragment() {

    lateinit var viewModel: BeerViewModel
    private val beerAdapter = BeerListAdapter(arrayListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel =
            BeerViewModel(BeerRepositoryImpl(MainActivity.beerService, MainActivity.database))

        viewModel.start()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_master, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        beersRecycler.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = beerAdapter
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }

        beersRecycler.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager = beersRecycler.layoutManager as LinearLayoutManager

                if (beerAdapter.itemCount == layoutManager.findLastCompletelyVisibleItemPosition() + 1) {
                    viewModel.nextPage()
                }
            }
        })

        swipeLayout.setOnRefreshListener {
            viewModel.start()
        }

        startObserving()
    }

    private fun startObserving() {
        viewModel.beers.observe(viewLifecycleOwner, Observer { beers ->
            beers?.let {
                lblTryAgain.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
                beerAdapter.updateBeers(it)
            }
        })

        viewModel.isLoading.observe(viewLifecycleOwner, Observer { isLoading ->
            isLoading?.let {
                if (swipeLayout.isRefreshing) {
                    swipeLayout.isRefreshing = it
                } else {
                    loadingBar.visibility = if (it) View.VISIBLE else View.GONE
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        activity.let { it!!.title = it.getString(R.string.app_name) }
    }
}