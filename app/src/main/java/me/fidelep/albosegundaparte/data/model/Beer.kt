package me.fidelep.albosegundaparte.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.Gson

@Entity
data class Beer(
    @PrimaryKey
    val id: Long,
    val name: String?,
    val tagline: String?,
    val image: String?,
    val description: String?,
    val firstBrewed: String?,
    val foodPairing: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeString(tagline)
        parcel.writeString(image)
        parcel.writeString(description)
        parcel.writeString(firstBrewed)
        parcel.writeString(foodPairing)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Beer> {
        override fun createFromParcel(parcel: Parcel): Beer {
            return Beer(parcel)
        }

        override fun newArray(size: Int): Array<Beer?> {
            return arrayOfNulls(size)
        }

        fun mapFromDTO(beer: BeerDTO): Beer {
            return Beer(
                beer.id,
                beer.name,
                beer.tagline,
                beer.image,
                beer.description,
                beer.firstBrewed,
                Gson().toJson(beer.foodPairing)
            )
        }
    }

}
