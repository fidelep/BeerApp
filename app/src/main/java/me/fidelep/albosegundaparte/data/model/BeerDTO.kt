package me.fidelep.albosegundaparte.data.model

import com.google.gson.annotations.SerializedName

data class BeerDTO(
    val id: Long,
    val name: String?,
    val tagline: String?,
    @SerializedName("image_url")
    val image: String?,
    val description: String?,
    @SerializedName("first_brewed")
    val firstBrewed: String?,
    @SerializedName("food_pairing")
    val foodPairing: List<String>?
)