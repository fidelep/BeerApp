package me.fidelep.albosegundaparte.data.api

import me.fidelep.albosegundaparte.data.model.BeerDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface BeerApi {

    @GET("beers")
    suspend fun getBeerByPage(@Query("page") page: Int, @Query("per_page") amount: Int): List<BeerDTO>
}