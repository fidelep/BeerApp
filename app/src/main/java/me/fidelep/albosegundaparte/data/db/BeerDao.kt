package me.fidelep.albosegundaparte.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import me.fidelep.albosegundaparte.data.model.Beer

@Dao
interface BeerDao {

    @Query("SELECT * FROM Beer")
    fun getStoredBeers(): List<Beer>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBeers(vararg beer: Beer)

    @Query("DELETE FROM Beer")
    fun clearBeers()
}