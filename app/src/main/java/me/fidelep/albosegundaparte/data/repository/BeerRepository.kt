package me.fidelep.albosegundaparte.data.repository

import androidx.lifecycle.LiveData
import me.fidelep.albosegundaparte.data.model.Beer

interface BeerRepository {
    suspend fun getBeersByPage(page: Int, amount: Int): LiveData<List<Beer>>
    var isLocal: Boolean
}