package me.fidelep.albosegundaparte.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import me.fidelep.albosegundaparte.data.model.Beer

@Database(entities = [(Beer::class)], version = 1)
abstract class BeerRoom : RoomDatabase() {
    abstract fun beerDao(): BeerDao
}