package me.fidelep.albosegundaparte.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import me.fidelep.albosegundaparte.data.api.BeerService
import me.fidelep.albosegundaparte.data.db.BeerRoom
import me.fidelep.albosegundaparte.data.model.Beer
import me.fidelep.albosegundaparte.data.model.BeerDTO
import java.net.UnknownHostException

class BeerRepositoryImpl(
    private val beerService: BeerService,
    private val beerDb: BeerRoom
) : BeerRepository {

    private val TAG = BeerRepositoryImpl::class.simpleName

    override var isLocal = false

    override suspend fun getBeersByPage(page: Int, amount: Int): LiveData<List<Beer>> =
        liveData {
            try {
                isLocal = false
                emit(fetchFromApi(page, amount))
            } catch (e: UnknownHostException) {
                logError("Host error. Trying with local data")
                isLocal = true
                emit(getCachedBeers())
            } catch (e: Exception) {
                val message = e.localizedMessage ?: "Error no identificado"
                logError(message)
                emit(emptyList())
            }
        }

    private suspend fun fetchFromApi(page: Int, beersPerPage: Int): List<Beer> =
        withContext(Dispatchers.IO) {
            val beers = mapBeers(beerService.getBeerByPage(page, beersPerPage))
            saveBeersToLocal(beers)
            return@withContext beers
        }


    private suspend fun saveBeersToLocal(beers: List<Beer>) = withContext(Dispatchers.IO) {
        beerDb.beerDao().insertBeers(*beers.toTypedArray())
    }

    private suspend fun getCachedBeers(): List<Beer> = withContext(Dispatchers.IO) {
        return@withContext beerDb.beerDao().getStoredBeers()
    }

    fun mapBeers(beers: List<BeerDTO>): List<Beer> {

        return beers.map {
            Beer.mapFromDTO(it)
        }

    }

    private fun logError(message: String) {
        Log.e(TAG, message)
    }

}