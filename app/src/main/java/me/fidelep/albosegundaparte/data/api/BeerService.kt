package me.fidelep.albosegundaparte.data.api

import android.util.Log
import me.fidelep.albosegundaparte.data.model.BeerDTO
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class BeerService {
    private val BASE_URL = "https://api.punkapi.com/v2/"
    private val api: BeerApi

    init {
        api = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
            .create(BeerApi::class.java)
    }

    suspend fun getBeerByPage(page: Int, amount: Int): List<BeerDTO> {
        return api.getBeerByPage(page, amount)
    }
}